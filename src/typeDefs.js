import { gql } from "apollo-server-core";


export const typeDefs = gql`
  type Query{
    globalResults: [Candidate!]!
    globalSummary: [Vote!]!
    outsideResults: [Candidate!]!
    outsideSummary: [Vote!]!
    chileResults: [Candidate!]!
    chileSummary: [Vote!]!
    continentResult(name: String!): [Candidate!]
    continentSummary(name: String!): [Vote!]
    countryResult(name: String!): [Candidate!]
    countrySummary(name: String!): [Vote!]
  },
  type Candidate{
    name: String!
    votes: String!
    percentage: String!
  }
  type Vote{
    type: String!
    quantity: String!
    percentage: String!
  }
`;
