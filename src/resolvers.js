import axios from 'axios';
import { ContinentID, CountryID} from './IDs.js'

const GLOBAL_API_URL = 'https://preliminares.servelelecciones.cl/data/elecciones_presidente/computo/global/19001.json'
const OUTSIDE_API_URL = 'https://preliminares.servelelecciones.cl/data/elecciones_presidente/computo/distribucion/10002.json'
const CHILE_API_URL = 'https://preliminares.servelelecciones.cl/data/elecciones_presidente/computo/pais/8056.json'

export const resolvers = {
  Query: {
    globalResults: () => results(GLOBAL_API_URL),
    globalSummary: () => summary(GLOBAL_API_URL),
    outsideResults: () => results(OUTSIDE_API_URL),
    outsideSummary: () => summary(OUTSIDE_API_URL),
    chileResults: () => results(CHILE_API_URL),
    chileSummary: () => summary(CHILE_API_URL),
    continentResult:  (_, {name}) => continentResult(name),
    continentSummary:  (_, {name}) => continentSummary(name),
    countryResult: (_, {name}) => countryResult(name),
    countrySummary: (_, {name}) => countrySummary(name),
  }
};


const results = async (API) =>{

  try{
    const res = await axios.get(API)

    const data = res.data['data'].map( el => {
      const { a:name, c:votes, d:percentage } = el 
      return { name, votes, percentage }
    })

    return data

  }catch(err){ 
    console.error(err) 
    return null
  }

}

const summary = async (API) => {
  try{
    const res = await axios.get(API)

    const data = res.data['resumen'].map( el => {
      const { a:type, c: quantity, d:percentage } = el
      return { type, quantity, percentage }
    })
    
    return data

  }catch(err){
    console.error(err)
    return null
  }
}


const continentResult = (arg) => {
  const ID  = ContinentID[arg.toUpperCase()]
  if ( !ID ) return null

  const API = `https://preliminares.servelelecciones.cl/data/elecciones_presidente/computo/continentes/${ID}.json`
  return results(API)
}

const continentSummary = (arg) => {
  const ID  = ContinentID[arg.toUpperCase()]
  if ( !ID ) return null

  const API = `https://preliminares.servelelecciones.cl/data/elecciones_presidente/computo/continentes/${ID}.json`
  return summary(API)
}

const countryResult = (arg) => {
  const ID = CountryID[arg.toUpperCase()]
  if (!ID) return null

  const API=`https://preliminares.servelelecciones.cl/data/elecciones_presidente/computo/pais/${ID}.json`
  return results(API)
}
const countrySummary = (arg) => {
  const ID = CountryID[arg.toUpperCase()]
  if (!ID) return null

  const API=`https://preliminares.servelelecciones.cl/data/elecciones_presidente/computo/pais/${ID}.json`
  return summary(API)
}

