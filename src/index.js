import { ApolloServer } from 'apollo-server-express';
import express from 'express';
import { typeDefs } from './typeDefs.js'
import { resolvers } from './resolvers.js'

const app = express();

const server = new ApolloServer({
  typeDefs,
  resolvers
});

server.applyMiddleware({ app });

app.listen({ port: process.env.PORT || 4000 }, () => {
  console.log(`🚀 Server ready at ${server.graphqlPath}`)
})

