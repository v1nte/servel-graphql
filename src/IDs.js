export const CountryID = {
    ALEMANIA: 8000,
  ARGENTINA: 8001,
  AUSTRALIA: 8002,
  AUSTRIA: 8003,
  BELGICA: 8004,
  BOLIVIA: 8005,
  BRASIL: 8006,
  CANADA: 8007,
  COLOMBIA: 8008,
  COREA: 8009,
  'COSTA RICA': 8010,
  CROACIA: 8011,
  CUBA: 8012,
  DINAMARCA: 8013,
  ECUADOR: 8014,
  EGIPTO: 8015,
  'EL LIBANO': 8016,
  'EL SALVADOR': 8017,
  'EMIRATOS ARABES UNIDOS': 8018,
  'ESPAÑA': 8019,
  'ESTADOS UNIDOS': 8020,
  'FEDERACION RUSA': 8021,
  FILIPINAS: 8022,
  FINLANDIA: 8023,
  FRANCIA: 8024,
  GRECIA: 8025,
  GUATEMALA: 8026,
  HAITI: 8027,
  HONDURAS: 8028,
  HUNGRIA: 8029,
  INDIA: 8030,
  INDONESIA: 8031,
  IRLANDA: 8032,
  ISRAEL: 8033,
  ITALIA: 8034,
  JAMAICA: 8035,
  JAPON: 8036,
  JORDANIA: 8037,
  KENIA: 8038,
  MALASIA: 8039,
  MARRUECOS: 8040,
  MEXICO: 8041,
  NICARAGUA: 8042,
  NORUEGA: 8043,
  'NUEVA ZELANDA': 8044,
  'PAISES BAJOS': 8045,
  PANAMA: 8046,
  PARAGUAY: 8047,
  PERU: 8048,
  POLONIA: 8049,
  PORTUGAL: 8050,
  'REINO UNIDO': 8051,
  'REPUBLICA CHECA': 8052,
  'REPUBLICA DOMINICANA': 8053,
  'REPUBLICA POPULAR CHINA': 8054,
  RUMANIA: 8055,
  SINGAPUR: 8057,
  SUDAFRICA: 8058,
  SUECIA: 8059,
  SUIZA: 8060,
  TAILANDIA: 8061,
  TURQUIA: 8062,
  URUGUAY: 8063,
  VENEZUELA: 8064,
  VIETNAM: 8065
}

export const ContinentID = {
  AFRICA: 15001,
  AMERICA: 15002,
  ASIA: 15003,
  EUROPA: 15004,
  OCEANIA : 15005
}
